<?php

	// module:module_aov_page
	
	if ( OOAddon::isAvailable('addon_oeggh_veranstaltungen') != 1 ) {
		echo 'Bitte installieren Sie das "Veranstaltungen" Addon';
	} else {
		if( !$REX['REDAXO'] ) {
			// FRONTEND Knowledge Library Output File
			include($REX["INCLUDE_PATH"].'/addons/addon_oeggh_veranstaltungen/outputs/aov_page.out.inc.php');
		} else {
			// BACKEND Knowledge Library Output File
			echo 'Ausgabe vom "Veranstaltungen" Addon!';
		}
	}

?>