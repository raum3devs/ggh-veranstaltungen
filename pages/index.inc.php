<?php


	// databases
	$db_aov_veranstaltungen	= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_veranstaltungen';
	$db_aov_kategorien 		= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_kategorien';


	// addon vars
	$myAddon = 'addon_oeggh_veranstaltungen';


	// vars
	$rowsPerPage = 500;
	$subStart = 'veranstaltungen';
	$subNow 	= rex_request('subpage', 'string', $subStart);
	$page 		= rex_request('page', 'string');
	$func 		= rex_request('func', 'string');
	$start 		= rex_request('start', 'int');
	$id 		= rex_request('id', 'int');
	$toggle		= rex_request('toggle', 'int');
	$name 		= rex_request ('name', 'string');


	// layout top
	include $REX['INCLUDE_PATH'].'/layout/top.php';

	// title
	$title_info = '<span style="font-size:14px; color:silver;">'.$REX['ADDON']['version'][$myAddon].'</span>';
	rex_title($REX['ADDON']['name'][$myAddon].' '.$title_info, $REX['ADDON'][$myAddon]['SUBPAGES']);
	

	// subpages
	if ($subNow == '' OR !in_array_multi($subNow, $REX['ADDON'][$myAddon]['SUBPAGES'])) {
		$subNow = $subStart;
	}
	$local_path = '/addons/' . $myAddon . '/pages/';
	require $REX['INCLUDE_PATH'] . $local_path . $subNow . '.inc.php';


	// layout bottom
	include $REX['INCLUDE_PATH'].'/layout/bottom.php';


?>
