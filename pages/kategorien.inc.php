<?php


	// vars
	$tbl 		= $db_aov_kategorien;
	
	
	// set status
	if( $func == 'status' ) {
		if ($id != 0) {
			echo change_not_inuse_status($tbl, $id, $tbl, 'category', $toggle, 'single');
		} $func = '';
	}
	
	
	// set delete
	if( $func == 'delete' ) {
		if ($id != 0) {
			echo delete_not_inuse_entry($tbl, $id, $tbl, 'category', 'single', $name);
			#echo $id.' to delete test';
		}
		$func = '';
	}


	// list output
	if ($func == '')
	{
        
		// add headline
		echo '<div class="rex-addon-output">';
		echo '<h2 class="rex-hl2">'.$I18N->msg('aov_kategorien').'</h2>';
		echo '</div>';

		// id, name, status, createdate, updatedate
		$query = 'SELECT id, name, status  FROM '.$tbl.' ORDER BY name ASC';
		$listName = $I18N->msg('aov_kategorien');
		$debug = FALSE;
		$list = new rex_list( $query, $rowsPerPage, $listName, $debug );

		// add new column
		$imgHeader = '<a href="'. $list->getUrl(array('func' => 'add')) .'" class="rex-i-element rex-i-metainfo-add"></a>';
		$list->addColumn($imgHeader, '<span class="rex-i-element rex-i-metainfo"></span>', 0, array( '<th class="rex-icon">###VALUE###</th>', '<td class="rex-icon">###VALUE###</td>' ) );
		$list->setColumnParams ( $imgHeader, array('func' => 'edit', 'id' => '###id###', 'start' => $start) );
		
		// sortable
		$list->setColumnSortable('id');
		$list->setColumnSortable('name');


		// colomn label
		$list->setColumnLabel('id', 'ID');
		$list->setColumnLabel('name', $I18N->msg('aov_kategorie'));
		$list->setColumnLabel('status', $I18N->msg('aov_status'));
		
		
		// add status function
		$list->setColumnParams('status', array('id' => '###id###', 'start' => $start));
		$list->setColumnFormat('status', 'custom', create_function(
				'$params', '$list = $params["list"];				
				$params = ($list->getValue("status") == "1") ? array("func" => "status", "id" => "###id###", "toggle" => "0") : array("func" => "status", "id" => "###id###", "toggle" => "1");
				return $list->getColumnLink("status", $list->getValue("status") != "1" ? "<span style=\'color: red;\'>Offline</span>" : "<span style=\'color: green;\'>Online</span>", $params);'
			)
		);

		// add delete
		$list->addColumn('delete',$I18N->msg('delete'),-1,array('<th>'.$I18N->msg('aov_delete').'</th>','<td>###VALUE###</td>'));
		$list->setColumnParams('delete', array('func' => 'delete', 'id' => '###id###', 'name' => '###name###', 'start' => $start));
		$list->addLinkAttribute('delete','onclick',"return confirm('".$I18N->msg('aov_delete_confirm', '###name###')."');");


		// list show
		$list->show();


	}
	// form output
	elseif ($func == 'edit' || $func == 'add')
	{
		// id, name, status, createdate, updatedate
		$tableName = $tbl;
		$fieldset = $I18N->msg('aov_kategorien');
		$whereCondition = 'id='.$id;
		$method = 'post';
		$debug = FALSE;
		$form = new custom_rex_form( $tableName, $fieldset, $whereCondition, $method, $debug, 'custom_rex_form' );
		#$form = new rex_form( $tableName, $fieldset, $whereCondition, $method, $debug );

		// add name
		$field = &$form->addTextField('name');
		$field->setLabel($I18N->msg('aov_name'));

		// add fieldset
		$form->addFieldset($I18N->msg('aov_settings'));

		// status
		$field = &$form->addSelectField('status');
		$field->setLabel($I18N->msg('aov_status'));
		$select = &$field->getSelect();
		$select->setSize(1);
		$select->addOption('Online',1);
		$select->addOption('Offline',0);
		$select->setAttribute('style','width: 100px');
		// Standardwert: 1
		if ($field->getValue()== '') {
			$field->setValue(0);
		}



		// add param
		$form->addParam('updatedate', rex_formatter :: format(time(), 'strftime', 'date'));
		if($func == 'edit') {
			$form->addParam('id', $id);
		}else{
			$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));			
		}
		$form->addParam('start', $start);


		// add fieldset
		$form->addFieldset($I18N->msg('aov_save'));

		// form show
		$form->show();

	}


?>
