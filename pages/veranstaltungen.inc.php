<?php


	// vars
	$tbl 		= $db_aov_veranstaltungen;
	
	
	// set status
	if( $func == 'status' ) {
		if ($id != 0) {
			echo change_not_inuse_status($tbl, $id, $tbl_files, 'category', $toggle, 'single');
		} $func = '';
	}
	
	
	// set delete
	if( $func == 'delete' ) {
		if ($id != 0) {
			echo delete_not_inuse_entry($tbl, $id, $tbl_files, 'category', 'single', $name);
			#echo $id.' to delete test';
		}
		$func = '';
	}


	// list output
	if ($func == '')
	{
        

		// add headline
		echo '<div class="rex-addon-output kl-headline">';
		echo '<h2 class="rex-hl2">'.$I18N->msg('aov').'</h2>';
		echo '</div>';


		// id, name, status, createdate, updatedate
		$query = 'SELECT id, datum_von, datum_bis, titel, status  FROM '.$tbl.' ORDER BY id ASC';
		$listName = $I18N->msg('aov');
		$debug = FALSE;
		$list = new rex_list( $query, $rowsPerPage, $listName, $debug );

	#	// column width
	#	$list->addTableColumnGroup(array(
	#		array('width' => 40),
	#		array('width' => 30),
	#		array('width' => 'auto'),
	#		array('width' => 45),
	#		array('width' => 70, 'span' => 2)
	#	));


		// add new column
		$imgHeader = '<a href="'. $list->getUrl(array('func' => 'add')) .'" class="rex-i-element rex-i-metainfo-add"></a>';
		$list->addColumn($imgHeader, '<span class="rex-i-element rex-i-metainfo"></span>', 0, array( '<th class="rex-icon">###VALUE###</th>', '<td class="rex-icon">###VALUE###</td>' ) );
		$list->setColumnParams ( $imgHeader, array('func' => 'edit', 'id' => '###id###', 'start' => $start) );


		// remove column
		$list->removeColumn('id');
		
		// sortable
		$list->setColumnSortable('id');
		$list->setColumnSortable('titel');


		// colomn label
		$list->setColumnLabel('id', 'ID');
		
		$list->setColumnLabel('datum_von', $I18N->msg('aov_datum_von'));
		$list->setColumnFormat('datum_von', 'custom', 'getDateByTimestamp');
		$list->setColumnParams('datum_von', array('ts' => '###datum_von###'));
		
		$list->setColumnLabel('datum_bis', $I18N->msg('aov_datum_bis'));
		$list->setColumnFormat('datum_bis', 'custom', 'getDateByTimestamp');
		$list->setColumnParams('datum_bis', array('ts' => '###datum_bis###'));
		
		$list->setColumnLabel('titel', $I18N->msg('aov_veranstaltung'));
		
		$list->setColumnLabel('status', $I18N->msg('aov_status'));
		
		
		// add status function
		$list->setColumnParams('status', array('id' => '###id###', 'start' => $start));
		$list->setColumnFormat('status', 'custom', create_function(
				'$params', '$list = $params["list"];				
				$params = ($list->getValue("status") == "1") ? array("func" => "status", "id" => "###id###", "toggle" => "0") : array("func" => "status", "id" => "###id###", "toggle" => "1");
				return $list->getColumnLink("status", $list->getValue("status") != "1" ? "<span style=\'color: red;\'>Offline</span>" : "<span style=\'color: green;\'>Online</span>", $params);'
			)
		);


		// add delete
		$list->addColumn('delete',$I18N->msg('delete'),-1,array('<th>'.$I18N->msg('aov_delete').'</th>','<td>###VALUE###</td>'));
		$list->setColumnParams('delete', array('func' => 'delete', 'id' => '###id###', 'name' => '###name###', 'start' => $start));
		$list->addLinkAttribute('delete','onclick',"return confirm('".$I18N->msg('aov_delete_confirm', '###name###')."');");


		// list show
		$list->show();


	}

	// form output
	elseif ($func == 'edit' || $func == 'add')
	{
		// id, name, status, createdate, updatedate
		$tableName = $tbl;
		$fieldset = $I18N->msg('aov');
		$whereCondition = 'id='.$id;
		$method = 'post';
		$debug = FALSE;
		$form = new custom_rex_form( $tableName, $fieldset, $whereCondition, $method, $debug, 'custom_rex_form' );
		#$form = new rex_form( $tableName, $fieldset, $whereCondition, $method, $debug );


		// add Titel
		$field = &$form->addTextField('titel');
			$field->setLabel($I18N->msg('aov_titel'));
		
		//add Text
		$field = &$form->addTextAreaField('text');
			$field->setLabel($I18N->msg('aov_text'));
			$field->setAttribute('class', 'tinyMCEEditor');
			
		$field = &$form->addTextAreaField('leitung');
			$field->setLabel($I18N->msg('aov_leitung'));
			$field->setAttribute('class', 'tinyMCEEditor');
			
		$field = &$form->addTextAreaField('veranstaltungsort');
			$field->setLabel($I18N->msg('aov_veranstaltungsort'));
			$field->setAttribute('class', 'tinyMCEEditor');
		
		//select Kategorie
		$field = &$form->addSelectField('kategorie');
		$field->setLabel($I18N->msg('aov_kategorie'));
		$select = &$field->getSelect();
		$select->setSize(1);
		$select->addOption('- - - -',0);
		$select->addSqlOptions("SELECT name, id FROM rex_22_kategorien WHERE status = 1");
		$select->setAttribute('style','width: 100px');
		if ($field->getValue()== '') {
			$field->setValue(0);
		}

		//add datum_von
		$field = &$form->addTextField('datum_von');
			$field->setLabel($I18N->msg('aov_datum_von'));
			$field->setAttribute('maxlength', 11);
			$field->setAttribute('size', 11);
			$field->setAttribute('id', 'datetimepicker_von');
			
			/*
			if ($field->getValue()== '') {
				$date = new DateTime(date('Y-m-d'));
				$date->modify('+0 day');
				$field->setValue($date->format('U'));
			}
			*/
		
		//add datum_bis
		$field = &$form->addTextField('datum_bis');
			$field->setLabel($I18N->msg('aov_datum_bis'));
			$field->setAttribute('maxlength', 11);
			$field->setAttribute('size', 11);
			$field->setAttribute('id', 'datetimepicker_bis');
			/*
			if ($field->getValue()== '') {
				$date = new DateTime(date('Y-m-d'));
				$date->modify('+0 day');
				$field->setValue($date->format('U'));
			}
			*/
		
		//add filelist
		$mediaCats = OOMediaCategory::getRootCategories();
		
		$field = &$form->addSelectField('files');
		$field->setLabel($I18N->msg('aov_dateien'));
		$select = &$field->getSelect();
		$select->setSize(10);
		$select->addOptgroup('Ohne Kategorie');
		$select->addSqlOptions("SELECT originalname, file_id FROM rex_file WHERE category_id = 0");
		
		foreach($mediaCats as $mCat){
			$select->addOptgroup($mCat->getName());
			$select->addSqlOptions("SELECT originalname, file_id FROM rex_file WHERE category_id = ".$mCat->getId());
		}
		
		//add org_logo
		$field = &$form->addMediaField('org_logo');
			$field->setLabel($I18N->msg('aov_org_logo'));
		
		// add fieldset
		$form->addFieldset($I18N->msg('aov_settings'));


		// status
		$field = &$form->addSelectField('status');
		$field->setLabel($I18N->msg('aov_status'));
		$select = &$field->getSelect();
		$select->setSize(1);
		$select->addOption('Online',1);
		$select->addOption('Offline',0);
		$select->setAttribute('style','width: 100px');
		// Standardwert: 1
		if ($field->getValue()== '') {
			$field->setValue(0);
		}


		// add param
		$form->addParam('updatedate', rex_formatter :: format(time(), 'strftime', 'date'));
		if($func == 'edit') {
			$form->addParam('id', $id);
		}else{
			$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));
		}
		$form->addParam('start', $start);


		// add fieldset
		$form->addFieldset($I18N->msg('aov_save'));

		// form show
		$form->show();

	}


?>
