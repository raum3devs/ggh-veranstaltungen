<?php
    // set path to the modules folder
    $path = $REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/modules/';
	
	$backendUrl = 'index.php?page='.$page.'&subpage='.$subNow;
	$getModuleName 	= rex_get('module_name', 'string', NULL);
	$getModuleId 	= rex_get('module_id', 'int', NULL);
	$getInstall 	= rex_get('install', 'int', NULL);
	
    // readdir and get all files to array
    $fileArr = readDirFiles($path);
	
	$modules = getModuleFiles($fileArr);
	
	// install
	if( !is_null($getInstall) AND $getInstall == 1) {
		
		if( isset($modules[$getModuleName]) ) {
			
			$in = rex_get_file_contents($path.$modules[$getModuleName]['in']);
			$out = rex_get_file_contents($path.$modules[$getModuleName]['out']);
			
			$mi = new rex_sql;
			$mi->debugsql = FALSE;
			$mi->setTable('rex_module');
			$mi->setValue('eingabe', addslashes($in));
			$mi->setValue('ausgabe', addslashes($out));
			
			if( !is_null($getInstall) AND $getModuleId == 0 ) {
				$mi->setValue('name', $getModuleName);
				$mi->insert();
				$module_id = (int) $mi->getLastId();
				echo rex_info('Modul "'.strtoupper($getModuleName).'" wurde installiert');
			} else {
				$mi->setWhere('id="' . $getModuleId . '"');
				$mi->update();
				echo rex_info('Modul "'.strtoupper($getModuleName).'" wurde aktualisiert');
			}		
		}
	}
	
	#print_pre($modules);
	
	// output the list
	$out = '';
	if( is_array($modules) AND count($modules) > 0 ) {
		// list found files
		foreach( $modules AS $key => $val ){
			
			$gm = new rex_sql();
			$gm->debugsql = FALSE;
			$gm->setQuery('SELECT * FROM rex_module where ausgabe LIKE "%' . $val['searchtext'] . '%"');
			
			$module_id = 0;
			$module_title = $key;
			$rexIconAdd = '-add';
			foreach ($gm->getArray() as $module) {
				$module_id = $module['id'];
				$module_title = $module['name'];
				$rexIconAdd = '';
			}
			
			$rexIcon = '<td class="rex-icon"><span">
						<span class="rex-i-element rex-i-module'.$rexIconAdd.'"></span></td>';
			
			$out .= '<tr>'.$rexIcon.'<td><strong>'.strtoupper($module_title).'</strong>';
			if( is_array($val) AND count($val) > 0 ) {
				$out .= ' (<br />';
				foreach( $val AS $mkey => $mval ) {
					$out .= '<strong style="padding-left:20px; display:inline-block; width: 70px;">'.$mkey.':</strong> <span style="color: #999;">'.$mval.'</span><br />';
				}
				$out .= ')';
			}
			
			
			//$urlParam = '&module_name='.$key.'&module_id='.$module_id.'&install=1';
			$urlParam = '&module_name='.$key.'&module_id='.$module_id.'&install=1';
			
			if(is_null($module_id) OR $module_id == 0 ) {
				$out .= '</td><td><a href="'.$backendUrl.$urlParam.'">Installieren</a></td></tr>';
			} else {
				$out .= '</td><td><a href="'.$backendUrl.$urlParam.'">Update</a></td></tr>';				
			}
			
		}
	} else {
		// no files found
		$out .= '<tr>'.$rexIcon.'<td colspan="2">Kein Modul vorhandne</td></tr>';
	}		
	
	echo 	'<table class="rex-table" summary="Auflistung aller angelegten Module">
			<caption>Liste der angelegten Module</caption>
			<colgroup><col width="40" /><col width="*" /><col width="80" /></colgroup>
			<thead><tr><th>&nbsp;</th><th>Modulbezeichnung</th><th>Funktionen</th></tr></thead>
			<tbody>'.$out.'</tbody>
			</table>';

?>
