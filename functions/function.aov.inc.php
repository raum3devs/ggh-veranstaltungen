<?php
	
	// Zum On/Off schalten eines Eintrags ---------------------------------------------------
	if(!function_exists('change_status'))
	{
		function change_status($tbl = NULL, $id, $toggle, $debug = FALSE)
		{
			if( !is_null($tbl) OR empty($tbl) ) {
				global $REX;
				$sql = new rex_sql();
				$sql->debugsql = $debug;
				$sql->setTable($tbl);
				$sql->setValue('status', $toggle);
				$sql->setWhere('id = '.intval($id));
				if($sql->update()) {			
					$new_status = ($toggle == 0) ? 'Offline' : 'Online';
					return rex_info('Status wurde f&uuml;r id:' . $id . ' aktualisiert. - ' . $new_status);				
				} else {
					return rex_warning('Status kann nicht ge&auml;ndert werden.');
				}
			}
			return FALSE;
		}
	}
	
	
	// Zum On/Off schalten eines Eintrags ---------------------------------------------------
	if(!function_exists('change_not_inuse_status'))
	{
		function change_not_inuse_status($tbl = NULL, $id, $searchtbl = NULL, $searchfield = 'id', $toggle, $what = 'single', $debug = FALSE)
		{
			global $REX;
			
			if( (!is_null($tbl) OR empty($tbl)) AND (!is_null($searchtbl) OR empty($searchtbl)) ) {
				switch($what){					
					// --------------------
					case 'multi': 	$searchqry = $searchfield.' LIKE ("%|'.$id.'|%")';
						break;
					// --------------------
					case 'single':
					default: 		$searchqry = $searchfield.' LIKE ("%'.$id.'%")';
						break;
					// --------------------						
				}
				
				// is id in use
				$useqry = 'SELECT id FROM '.$searchtbl.' WHERE '.$searchqry.' AND status = 1 ORDER BY id';				
				$sql = rex_sql::factory();
				$sql->debugsql = $debug;
				$sql->setQuery($useqry);
				$rows = $sql->getRows();
				
				if($rows > 0 AND $toggle == 0) {
					return rex_warning('Der Status kann nicht ge&auml;ndert werden. Dieser Eintrag ist "'.$rows.'" mal in verwendung.');
				} else {
					$sql = new rex_sql();
					$sql->debugsql = $debug;
					$sql->setTable($tbl);
					$sql->setValue('status', $toggle);
					$sql->setWhere('id = '.intval($id));
					if($sql->update()){
						$new_status = ($toggle == 0) ? 'Offline' : 'Online';
						return rex_info('Status wurde f&uuml;r id:' . $id . ' aktualisiert. - ' . $new_status);	
					} else {
						return rex_warning('Status kann nicht ge&auml;ndert werden.');
					}
				}
				
			}
			return FALSE;
		}
	}
	
	if(!function_exists('delete_not_inuse_entry'))
	{
		function delete_not_inuse_entry($tbl = NULL, $id, $searchtbl = NULL, $searchfield = 'id', $what = 'single', $name = 'Eintrag', $debug = FALSE)
		{
			global $REX;
			
			if( (!is_null($tbl) OR empty($tbl)) AND (!is_null($searchtbl) OR empty($searchtbl)) ) {
				switch($what){					
					// --------------------
					case 'multi': 	$searchqry = $searchfield.' LIKE ("%|'.$id.'|%")';
						break;
					// --------------------
					case 'single':
					default: 		$searchqry = $searchfield.' LIKE ("%'.$id.'%")';
						break;
					// --------------------						
				}
				
				// is id in use
				$useqry = 'SELECT id FROM '.$searchtbl.' WHERE '.$searchqry.' AND status = 1 ORDER BY id';				
				$sql = rex_sql::factory();
				$sql->debugsql = $debug;
				$sql->setQuery($useqry);
				$rows = $sql->getRows();
				
				if($rows > 0) {
					return rex_warning($name.' kann nicht gel&ouml;scht werden. Dieser Eintrag ist "'.$rows.'" mal in verwendung.');
				} else {
					$sql = new rex_sql();
					$sql->debugsql = $debug;
					
					$sql->setTable($tbl);
					#$sql->setValue('status', $toggle);
					$sql->setWhere('id = '.intval($id));
					if($sql->delete()){
						return rex_info($name.' wurde gel&ouml;scht.');	
					} else {
						return rex_warning($name.' kann nicht gel&ouml;scht werden.');
					}
					
				}
				
			}
			return FALSE;
		}
	}
	
	
	// in_array_multi ------------------------------------------------------------------------------
	if( !function_exists('in_array_multi') ) {
		function in_array_multi($needle, $haystack, $strict = false) {
			foreach ($haystack as $item) {
				if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_multi($needle, $item, $strict))) {
					return true;
				}
			}
			return false;
		}
	}
	// in_array_multi ------------------------------------------------------------------------------


	// print_pre -----------------------------------------------------------------------------------
	if( !function_exists('print_pre') ) {
		function print_pre($val, $subclass = '', $style = NULL){
			echo '<pre class="print_pre '.$subclass.'" style="'.$style.'">';
			print_r($val);
			echo  '</pre>';
		}
	}
	// print_pre -----------------------------------------------------------------------------------
	
	
	
	// aov_backend_files ----------------------------------------------------------------------------
	if (!function_exists('aov_backend_files')) {
		function aov_backend_files($params) {
			global $REX;
			$page 		= rex_request('page', 'string');
			$subpage 	= rex_request('subpage', 'string');
			
			if( $page == 'addon_oeggh_veranstaltungen' ) {
				if( $subpage == 'veranstaltungen' ) {
					echo '
						<script type="text/javascript" src="../files/addons/addon_oeggh_veranstaltungen/vendor/datetimepicker/jquery.js"></script>
						<script type="text/javascript" src="../files/addons/addon_oeggh_veranstaltungen/vendor/datetimepicker/jquery.datetimepicker.js"></script>
						<link type="text/css" rel="stylesheet" href="../files/addons/addon_oeggh_veranstaltungen/vendor/datetimepicker/jquery.datetimepicker.css" />
						<script type="text/javascript" src="../files/addons/addon_oeggh_veranstaltungen/js/aov_backend_init.js"></script>
					';
				}
				/*
				if( $subpage == 'manage_rel' ) {
					echo '
						<link type="text/css" rel="stylesheet" href="../files/addons/addon_oeggh_veranstaltungen/css/aov_backend_design.css" />
					';
				}
				*/
			}
		}
	}
	// aov_backend_files ----------------------------------------------------------------------------
	
	

	// aov_frontend_files ---------------------------------------------------------------------------
	if (!function_exists('aov_frontend_files')) {
		function aov_frontend_files($params) {
			global $REX;
			
			$insert = '';
			#$insert .= '<link type="text/css" rel="stylesheet" href="./files/addons/addon_oeggh_veranstaltungen/css/kl_frontend_design.css" />';
			$insert .= '<link type="text/css" rel="stylesheet" href="'.seo42::getCSSFile('../../files/addons/addon_oeggh_veranstaltungen/css/aov_frontend_design.less').'" />';
			$insert .= '<script type="text/javascript" src="./files/addons/addon_oeggh_veranstaltungen/js/aov_frontend_init.js"></script>';
			$search = '</head>';
			$replace = $insert.'</head>';
			return str_replace($search, $replace, $params['subject']);			
		}
	}
	// aov_frontend_files ---------------------------------------------------------------------------
	
	
	
	// getModuleFiles ------------------------------------------------------------------------------
	if (!function_exists('readDirFiles')) {
		function readDirFiles($path = NULL) {
			$arr = '';
			if( !is_null($path) ) {
				if ($handle = opendir($path)) {
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." AND $entry != "..") {
							$arr[] = $entry;
						}
					}		
					closedir($handle);
				}
				return $arr;
			}
			return FALSE;
		}
	}
	// getModuleFiles ------------------------------------------------------------------------------
	
	
	
	// getModuleFiles ------------------------------------------------------------------------------
	if (!function_exists('getModuleFiles')) {
		function getModuleFiles($fileArr = NULL) {
			$arr = '';
			if( !is_null($fileArr) AND is_array($fileArr) ) {
				foreach( $fileArr AS $key => $var ) {
					$entryArr = explode('.',$var);
					if( is_array($entryArr) AND count($entryArr) == 3 ) {
						list($m_name,$m_what,$m_ending) = $entryArr;
						if( $m_ending == 'inc' AND ($m_what == 'in' OR $m_what == 'out') ) {
							$arr[$m_name]['searchtext'] = '// module:'.$m_name;
							$arr[$m_name][$m_what] = $var;
						}
					}
				}
				return $arr;
			}
			return FALSE;
		}
	}
	// getModuleFiles ------------------------------------------------------------------------------

	
	
	// rip_tags ------------------------------------------------------------------------------------
	// http://php.net/manual/de/function.strip-tags.php#110280
	if (!function_exists('rip_tags')) {
		function rip_tags($string) {   
			// ----- remove HTML TAGs -----
			$string = preg_replace ('/<[^>]*>/', ' ', $string);
		   
			// ----- remove control characters -----
			$string = str_replace("\r", '', $string);    // --- replace with empty space
			$string = str_replace("\n", ' ', $string);   // --- replace with space
			$string = str_replace("\t", ' ', $string);   // --- replace with space
		   
			// ----- remove multiple spaces -----
			$string = trim(preg_replace('/ {2,}/', ' ', $string));
		   
			return $string;		
		}
	}
	// rip_tags ------------------------------------------------------------------------------------
	
	
	// shortText -----------------------------------------------------------------------------------
	// http://www.hoerandl.com/code-schnipsel/php-codes/string-funktionen/item/einen-lange-text-auf-eine-bestimmte-laenge-kuerzen
	if (!function_exists('shortText')) {
		function shortText($string,$lenght) {
			$string = rip_tags($string);
			if(strlen($string) > $lenght) {
				$string = substr($string,0,$lenght)."...";
				$string_ende = strrchr($string, " ");
				$string = str_replace($string_ende," ...", $string);
			}
			return $string;
		}
	}
	// shortText -----------------------------------------------------------------------------------
	
	
	// addhttp -------------------------------------------------------------------------------------
	if( !function_exists('addhttp') ) {
		function addhttp($url) {
			if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
				$url = "http://" . $url;
			}
			return $url;
		}
	}
	// addhttp -------------------------------------------------------------------------------------

	
	if(!function_exists('getDateByTimestamp')){
		function getDateByTimestamp($ts = 0){
			if($ts != 0){
				return date('d.m.Y', $ts['value']);
			}
		}
	}
?>
