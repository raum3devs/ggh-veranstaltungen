<?php


	// addon vars
	$myAddon 			= 'addon_oeggh_veranstaltungen';
	$myAddonPerm 		= 'veranstaltungen[]';
	$myAddonPermSetup 	= 'veranstaltungen[setup]';
	$myAddonName 		= 'Veranstaltungen';


	// append lang file
	if ($REX['REDAXO']) {
		// backend
		// use -> $I18N->msg('YOUR_TEXT_PATTERN');
		$I18N->appendFile($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/lang/');
		
		$myAddonName = $I18N->msg('aov');
	}


	// register addon
	$REX['ADDON']['rxid'][$myAddon] = '22';
	$REX['ADDON']['name'][$myAddon] = $myAddonName;
	$REX['ADDON']['version'][$myAddon] = '0.1';
	$REX['ADDON']['date'][$myAddon] = '09.09.2015 - 0928';
	$REX['ADDON']['update'][$myAddon] = '-';
	$REX['ADDON']['page'][$myAddon] = $myAddon;
	$REX['ADDON']['author'][$myAddon] = 'Thomas Schmidt';
	$REX['ADDON']['supportpage']['seo42'] = 'www.raum3.at';
	$REX['ADDON']['perm'][$myAddon] = $myAddonPerm;


	// permissions
	$REX['PERM'][] = $myAddonPerm;
	$REX['PERM'][] = $myAddonPermSetup;


	// consts
	#define('MY_ADDON', $myAddon);


	// includes
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/functions/function.aov.inc.php');
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/classes/class.aov.inc.php');
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/classes/class.custom_rex_form.inc.php');
	
	if ($REX['REDAXO']) {
		// backend
		rex_register_extension('PAGE_HEADER', 'aov_backend_files');
		
	} else {
		// frontend		
		rex_register_extension('ADDONS_INCLUDED', 'aov::init');
	}

	if ($REX['REDAXO']) {
		// subpages
		$subpages = array(
			// translate
			array('veranstaltungen',	$I18N->msg('aov')),
			array('kategorien',	$I18N->msg('aov_kategorien')),
			array('setup',	$I18N->msg('aov_setup'))
		);
		// setup perm
		if (isset($REX['USER']) AND $REX['USER']->isAdmin() AND $REX['USER']->hasPerm($myAddonPermSetup) ) {
			array_push($subpages, array('setup', $I18N->msg('aov_setup')) );
		}
		
		$REX['ADDON'][$myAddon]['SUBPAGES'] = $subpages;
		
	}


?>
