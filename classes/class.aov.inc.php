<?php

	class aov {
		
		protected static $maxMonths		= 12;	
		protected static $monthsNames 	= array('Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez');
		protected static $dayNames 		= array('Mo','Di','Mi','Do','Fr','Sa','So');
		
		
		public static function init(){
			
			// GET JSON OF THE CALENDAR DATA ---------------------------------------------------------------
			
			
			$year = rex_get('y', 'int', null);
			$getWhat = rex_get('func', 'string', '');
			$date = rex_get('date', 'string', '');
			
			
			if($year != null && is_numeric($year) && $getWhat != ''){
				if(is_numeric($year)){
					switch($getWhat){
						case 'calendar':
							$data = self::getYearArr($year);
							$type = 'application/json';
							break;
						case 'holiday':
							$data = self::getHoliday($year);
							$type = 'application/json';
							break;
						case 'eventDays':
							$data = self::getEventDaysAsJSON($year);
							$type = 'application/json';
							break;
						case 'eventsForDate':
							$data = self::getEventsOverlay(self::getEventsForDate($date));
							$type = 'text/html; charset=utf-8';
							break;
					}
					$header = 'HTTP/1.1 200 Ok';
				}else{
					$header = 'HTTP/1.0 404 Not Found';
					$data = json_encode(array('calendar'=>'error'));
				}
				
				header('Cache-Control: no-cache, must-revalidate');
				header('Content-type: '.$type);
				header($header);
				echo $data;
				exit;
			}
		}
		
		// getYearArr ------------------------------------------------------------------------------
		public static function getYearArr($year = NULL, $type = NULL) 
		{
			if( !is_null($year) AND is_numeric($year) ) {
				
				if( $type == 'array' ) {
					return static::setYearArr($year);
				} else {
					return utf8_encode(json_encode(static::setYearArr($year)));
				}
			} else {
				exit;
			}
		}
		
		// setYearArr ------------------------------------------------------------------------------
		private function setYearArr($year) 
		{
			$yearArr 	= array();
			$monthsArr 	= array();
			
			if( $year > 2037 ) { $year = 2037; }
			if( $year < 1970 ) { $year = 1970; }
			
			for( $m = 1; $m <= static::$maxMonths; $m++ ) {
				$maxDays 		= date('t', mktime(0, 0, 0, $m, 1, $year));
				$firstDayName 	= date('N', mktime(0, 0, 0, $m, 1, $year));				
				$monthsArr[] = array(
					'm'	=> intval($m),
					'd'	=> intval($maxDays),
					'wd'=> intval($firstDayName)
				);
			}			
			$yearArr['calendar'] = array(
				'today' => date('Y-m-d'),
				'year' 	=> intval($year),
				'months' => $monthsArr
			);
			
			return $yearArr;
			
		}
		
		public static function getHoliday($year = NULL, $type = NULL) 
		{
			if( !is_null($year) AND is_numeric($year) ) {
				;
				if( $type == 'array' ) {
					return static::getHolidayArray($year);
				} else {
					return utf8_encode(json_encode(static::getHolidayArray($year)));
				}
			} else {
				exit;
			}
		}
		
		// setHolidayArray -------------------------------------------------------------------------
		private static function setHolidayArray( $year = NULL )
		{
			// Bewegliche Feiertage berechnen
			$days = 60 * 60 * 24;
			$eastersunday = easter_date($year);

			$holiday = array(
				// http://www.feiertage-oesterreich.at/2014/
				// Feste Feiertage werden nach dem Schema ddmm eingetragen
				array('0101', 'Neujahrstag'),
				array('0601', 'Dreikönigstag'),
				array('0105', 'Staatsfeiertag'),
				array('1508', 'Maria Himmelfahrt'),
				array('2610', 'Nationalfeiertag'),
				array('0111', 'Allerheiligen'),
				array('0812', 'Maria Empfängnis'),
				array('2512', 'Erster Weihnachtstag'),
				array('2612', 'Zweiter Weihnachtstag'),
				array('3112', 'Silvester'),
				// Bewegliche Feiertage berechnen
				array(date("dm", $eastersunday + 2  * $days), 'Ostermontag'),
				array(date("dm", $eastersunday + 40 * $days), 'Himmelfahrt'),
				array(date("dm", $eastersunday + 51 * $days), 'Pfingstmontag'),
				array(date("dm", $eastersunday + 61 * $days), 'Fronleichnam')
			);
			return $holiday;
		}
		
		// GET HolidayArray ------------------------------------------------------------------------
		private static function getHolidayArray( $year = NULL )
		{
			if( $year == '' ) {
				$year = date('Y');
			}
			
			$holiday 	= static::setHolidayArray($year);
			$arr 		= array();

			foreach ( $holiday AS $key => $var ) {
				$d = substr($var[0],  0, 2);
				$m = substr($var[0], -2, 2);
				$y = $year;

				$ts = mktime(0, 0, 0, $m, $d, $y);
				
				$name = $var[1];

				$arr['holiday'][$key] = array(
						//'ts' 	=> $ts,
						'date' 	=> $y.'-'.$m.'-'.$d,
						'name' 	=> $name
					);
			}
			return $arr;
		}
		
		/**
		 * getEvents gibt alle Veranstaltungen des übergebenen Jahres zurück
		 */
		public static function getEventDaysAsJSON($year = null){
			if($year != null){
				$sql = rex_sql::factory();
				$sql->setQuery("SELECT id FROM rex_22_kategorien WHERE name='$year'");
				$result = $sql->getArray();
				
				$sql->setQuery("SELECT datum_von, datum_bis FROM rex_22_veranstaltungen WHERE status = 1 AND kategorie = ".$result[0]['id']);
				$result = $sql->getArray();
				
				$return = array();
				
				for($i = 0; $i < count($result); $i++){
					$df = strtotime(date('Y-m-d', $result[$i]['datum_von']));
					$dt = strtotime(date('Y-m-d', $result[$i]['datum_bis']));
					
					$return[] = date('Y-m-d', $result[$i]['datum_von']);
					
					if(($dt-$df)/86400 >= 2){
						while($df != $dt-86400){
							$df += 86400;
							$return[] = date('Y-m-d', $df);
						}
					}
					
					$return[] = date('Y-m-d', $result[$i]['datum_bis']);
				}
				return utf8_encode(json_encode(array_unique($return)));
			}
		}
		
		/**
		 * getEventsForDate gibt die Daten der Veranstaltung
		 * des übergebenen Datums zurück
		 */
		public static function getEventsForDate($date = null){
			if($date != null){
				
				$dayTS = strtotime($date);
				
				$sql = rex_sql::factory();
				$sql->setQuery("SELECT titel, text, leitung, veranstaltungsort, files, datum_von, datum_bis FROM rex_22_veranstaltungen WHERE $dayTS >= datum_von AND $dayTS <= datum_bis");
				
				$result = $sql->getArray();
				
				for($i = 0; $i < count($result); $i++){
					$result[$i]['datum_von'] = date('d.m.Y', $result[$i]['datum_von']);
					$result[$i]['datum_bis'] = date('d.m.Y', $result[$i]['datum_bis']);
				}
				
				return $result;
				
			}else{
				return utf8_encode(json_encode());
			}
		}
		
		/**
		 * getEventsOverlay gibt das Overlay mit
		 * den übergebenen Veranstaltungen zurück
		 */
		public static function getEventsOverlay($data){
			
			$out = '';
			$eventList = '';
			$datum = '';
			
			$i = 1;
			foreach($data as $event){
				$eventList .= '<li>';
				
				if($event['datum_von'] == $event['datum_bis']){
					$datum = $event['datum_von'];
				}else{
					$datePartsFrom = explode('.', $event['datum_von']);
					$datePartsTo = explode('.', $event['datum_bis']);
					
					if($datePartsFrom[2] == $datePartsTo[2]){
						$datum = $datePartsFrom[0].'.'.$datePartsFrom[1].'.-'.$event['datum_bis'];
					}elseif($datePartsFrom[1] == $datePartsTo[1] && $datePartsFrom[2] == $datePartsTo[2]){
						$datum = $datePartsFrom[0].'.-'.$event['datum_bis'];
					}else{
						$datum = $event['datum_von'].'-'.$event['datum_bis'];
					}
				}
				
				$eventList .= '<span class="cal-event-overlay-list-date">'.$datum.'</span><span class="cal-event-overlay-list-title">'.$event['titel'].'</span>';
				$eventList .= '</li>
								<div class="cal-event-overlay-list-details">
								<div class="cal-event-overlay-list-details-title">'.$event['titel'].'</div>
								<div class="cal-event-overlay-list-details-date"><span>Datum</span>'.$datum.'</div>';
				
				if($event['veranstaltungsort'] != ''){
					$eventList .= '<div class="cal-event-overlay-list-details-venue"><span>Veranstaltungsort</span>'.$event['veranstaltungsort'].'</div>';
				}
				if($event['leitung'] != ''){
					$eventList .= '<div class="cal-event-overlay-list-details-admin"><span>Leiter</span>'.$event['leitung'].'</div>';
				}
				if($event['files'] != ''){
					$media = OOMedia::getMediaById($event['files']);
					if($media->getTitle() != ''){
						$filename = $media->getTitle();
					}else{
						$filename = $media->getFileName();
					}
					$file = $media->getFileName();
					$eventList .= '<div class="cal-event-overlay-list-details-files"><span class="file-icon"></span><div><span>'.$filename.'</span><a href="'.seo42::getDownloadFile($file).'">download</a></div></div>';
				}
				
					$eventList .= '<div class="cal-event-overlay-list-details-text">'.$event['text'].'</div>';
				$eventList .=	'</div>';
				$i++;
			}
			
			$out .= '<div class="cal-event-overlay">
						<div class="cal-event-overlay-bg"></div>
						<div class="cal-event-overlay-inner">
								<ul>
									'.$eventList.'
								</ul>
						</div>
					</div>
					';
			
			return $out;
		}
	}

?>
