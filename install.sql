DROP TABLE IF EXISTS `%TABLE_PREFIX%22_veranstaltungen`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%22_kategorien`;

/* rex_22_veranstaltungen */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%22_veranstaltungen` (
	`id` int(11) NOT NULL auto_increment,
	`titel` TEXT NOT NULL default '',
	`text` TEXT NOT NULL default '',
	`kategorie` int(11) NOT NULL default '0',
	
	`leitung` TEXT NOT NULL default '',
	`veranstaltungsort` TEXT NOT NULL default '',
	
	`datum_von` int(11) NOT NULL default '0',
	`datum_bis` int(11) NOT NULL default '0',
	`files` TEXT NOT NULL default '',
	`org_logo` varchar(255) NOT NULL default '',
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`updatedate` int(11) NOT NULL,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* rex_22_kategorien */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%22_kategorien` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`updatedate` int(11) NOT NULL,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;