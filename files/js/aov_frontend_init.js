$( document ).ready(function() {
	// http://api.jquery.com/jQuery.getJSON/

	var monthsNames = ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'];
	var dayNames 	= ['Mo','Di','Mi','Do','Fr','Sa','So'];			
	
	var date = new Date();
	var yearNow = date.getFullYear();
	
	$('#selectField').on('change', function(){
		getCalendar($(this).val());
	});
	
	getCalendar(yearNow);
	
	$('#cal').delegate('.event', 'click', function(e){
		e.preventDefault();
		var day = $(this).attr('href').replace('#', '');
		$('.cal-event-overlay').remove();
		$.get('index.php', {y:yearNow, func:'eventsForDate', date: day}, function( data ){
			$('#cal').append(data);
		});
	});
	
	$('#cal').delegate('.cal-event-overlay-bg', 'click', function(){
		$('.cal-event-overlay').remove();
		$('.cal-event-overlay-bg').remove();
		$('.cal-event-overlay-inner').remove();
	});
	
	$('#cal').delegate('.cal-event-overlay-inner li', 'click', function(e){
		
		if (!$(this).next('.cal-event-overlay-list-details').hasClass('open')) {
			$('.bt').toggleClass('bt');
			$('.open').slideUp().removeClass('open');
			$(this).next('.cal-event-overlay-list-details').slideDown().addClass('open');
			$(this).next().next().addClass('bt');
		}else{
			$('.bt').toggleClass('bt');
			$('.open').slideUp().removeClass('open');
		}
	});
	
	function getCalendar(getYear) {
		//get calendar data
		$.getJSON( 'index.php', {y:getYear, func:'calendar'}, function( data ) {
			var items = [];
			var year = data.calendar.year;
			var today = data.calendar.today;
			
			items.push('<div class="calendar-inner">');
			items.push('<div class="cal-head">'+year+'</div>');
			$.each( data.calendar.months, function( key, val ) {
				var monthsNumber 	= val.m-1;
				var dayNameNumber 	= val.wd-1;
				
				items.push('<div class="cal-month">');
				items.push('<div class="cal-month-head">'+monthsNames[monthsNumber]+' <span class="small">('+val.d+')</span>'+'</div>');
				
				for(var i = 0; i < dayNameNumber; i++){
					items.push('<div class="placeholderDay"></div>');
				}
				
				for( var d=1; d<=val.d; d++ ){
					
					var wdClass = '';
					
					if (dayNameNumber == 5 || dayNameNumber == 6) { wdClass = 'wd'; }
					
					var addMonthZero = '';
					if (val.m < 10) { addMonthZero = 0; }	
					
					var addDayZero = '';
					if (d < 10) { addDayZero = 0; }
					
					linkHash = year+'-'+addMonthZero+val.m+'-'+addDayZero+d;
					
					var todayClass = '';
					if (linkHash == today ) {
						todayClass = 'today';
					}
					
					items.push(
						'<a href="#'+linkHash+'" id="day_'+linkHash+'" class="cal-day '+wdClass+' '+todayClass+'">'+
								d+' '+dayNames[dayNameNumber]+
						'</a>'
					);
					
					if (dayNameNumber == 6) {
						dayNameNumber = 0;
					} else {
						dayNameNumber++;
					}
				}
				items.push('</div>');
			});
			items.push('<div class="clear"></div>');
			items.push('</div>');
			
			$('.calendar').remove();
			$( "<div/>", {
				'id': 'cal_'+year,
				'class': 'calendar',
				html: items.join('')
			}).appendTo('#cal');
			
			//get holiday data
			$.getJSON( 'index.php', {y:getYear, func:'holiday'}, function( data ) {
				$.each( data.holiday, function( key, val ) {
					$('#day_'+val.date)
					.addClass('holiday')
					//.attr('title',unescape(val.name));
					.attr('title',val.name);
				});
			});
			
			//get event data
			$.getJSON('index.php', {y:getYear, func:'eventDays'}, function( data ){
				$.each( data , function(key, val){
					
					$('#day_'+val).addClass('event');
				});
			});
			
		});
	}
});	