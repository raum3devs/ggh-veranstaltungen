$(document).ready(function(){
	
	jQuery(function($){
	
	// Start Veranstaltungen
	$('#datetimepicker_von').datetimepicker({
		lang:'de',
		timepicker:true,
		defaultTime:'00:00:00',
		formatTime: 'H:i:s',
		allowTimes:[
			'00:00:00'
		],
		inline: true,
		format:'unixtime'
	});
	
	$('#datetimepicker_bis').datetimepicker({
		lang:'de',
		timepicker:true,
		defaultTime:'00:00:00',
		formatTime: 'H:i:s',
		allowTimes:[
			'00:00:00'
		],
		inline: true,
		format:'unixtime'
	});

	// Ende Veranstaltungen
	
});

	
});
